package com.sourcekod.injection.prototype2singleton;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.sourcekod.injection")
public class AppConfig {
	
	@Bean
	public SingletonBean singletonBean() {
		return new SingletonBean();
	}
	/**
	 *  Lookup methods cannot get replaced on beans returned from factory methods where we can't dynamically provide a subclass for them.
	 *  So, remove the following factory style bean definition and add @Component annotation to SingletonLookupBean class.
	 *  @see https://stackoverflow.com/questions/26028341/how-to-use-spring-lookup-annotation
	 * */
//	@Bean
//	public SingletonLookupBean singletonLookupBean() {
//		return new SingletonLookupBean();
//	}
	
	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public PrototypeBean prototypeBean() {
		return new PrototypeBean();
	}
}
