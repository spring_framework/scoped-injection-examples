package com.sourcekod.injection.prototype2singleton;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
/**
 * In this main class we are getting the SingletonBean twice from the application context 
 * and getting the autowired PrototypeBean from theseSingletonBeans using the related getter method.
 * As you can see in the console logs, both Singleton and Prototype beans are created only once. But we want to get a new instance for 
 * our Prototype scoped beans whenever we request them from within our Singleton beans. <br/><br/>
 * I will demonstrate how we can achieve this.
 * */

public class Main {
	
	public static void main(String[] args) throws InterruptedException {
		AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		SingletonBean SB1 = appContext.getBean(SingletonBean.class);
		PrototypeBean PB1 = SB1.getPrototypeBean();
		Thread.sleep(100);
		SingletonBean SB2 = appContext.getBean(SingletonBean.class);
		PrototypeBean PB2 = SB2.getPrototypeBean();
		
		if (PB1.equals(PB2)) {
			System.out.println(">>> Both prototype beans are the same");
		}
		
		//== LOOKUP ==
		
		SingletonLookupBean slb3 = appContext.getBean(SingletonLookupBean.class);
		PrototypeBean PB3 = slb3.getPrototypeBean();
		SingletonLookupBean slb4 = appContext.getBean(SingletonLookupBean.class);
		PrototypeBean PB4 = slb4.getPrototypeBean();
		
		/**
		 * As we can see in the console, this time PrototypeBean instances are not the same which is what we want to achieve for 
		 * Prototype scoped beans.
		 * */
		if (PB3.equals(PB4)) {
			System.out.println(">>> Both prototype beans are the same");
		}else {
			System.out.println("PB3 and PB4 are different bean instances.");
		}
		
		// == OBJECT FACTORY == 
		
		SingletonObjectFactoryBean slb5 = appContext.getBean(SingletonObjectFactoryBean.class);
		PrototypeBean PB5 = slb5.getPrototypeBean();
		SingletonObjectFactoryBean slb6 = appContext.getBean(SingletonObjectFactoryBean.class);
		PrototypeBean PB6 = slb6.getPrototypeBean();
		
		/**
		 *  
		 * As we can see in the console, this time PrototypeBean instances are not the same which is what we want to achieve for 
		 * Prototype scoped beans.<br/>
		 * @see SingletonObjectFactoryBean
		 * */
		if (PB5.equals(PB6)) {
			System.out.println(">>> Both prototype beans are the same");
		}else {
			System.out.println("PB5 and PB6 are different bean instances.");
		}
		
		appContext.close();
	}
}
