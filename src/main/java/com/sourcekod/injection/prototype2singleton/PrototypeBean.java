package com.sourcekod.injection.prototype2singleton;

public class PrototypeBean {
	public PrototypeBean() {
		System.out.println(">>>PrototypeBean instance created.");
	}
}
