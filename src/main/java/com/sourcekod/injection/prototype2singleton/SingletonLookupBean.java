package com.sourcekod.injection.prototype2singleton;

import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

/**
 * To be able to use the @Lookup annotation, this bean should be instantiated by the container, not with factory methods. 
 * So we annotate this class with @Component instead of returning an instance in AppConfig class.
 * */
@Component
public class SingletonLookupBean {
	
	public SingletonLookupBean() {
		System.out.println(">>>SingletonLookupBean instance created.");
	}
	
	@Lookup
	public PrototypeBean getPrototypeBean() {
		return null;
	}
}
