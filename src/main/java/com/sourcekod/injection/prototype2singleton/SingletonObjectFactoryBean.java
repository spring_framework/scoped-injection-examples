package com.sourcekod.injection.prototype2singleton;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This time I used the ObjectFactory Interface which encapsulates a generic factory which return a new instance of some target object.
 * @see https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/ObjectFactory.html
 * */
@Component
public class SingletonObjectFactoryBean {
	
	@Autowired 
	private ObjectFactory<PrototypeBean> prototypeBeanObjectFactory;
	
	public PrototypeBean getPrototypeBean() {
		return prototypeBeanObjectFactory.getObject();
	}
}
